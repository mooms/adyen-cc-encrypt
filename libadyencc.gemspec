Gem::Specification.new do |s|
  s.name    = "libadyencc"
  s.version = "0.4.0"
  s.summary = "Adyen Creditcard Encryption"
  s.author  = "Matthijs Ooms"
  s.description = "Adyen Creditcard Encryption for client side encryption of creditcard details"
  s.email = "%s"
  s.homepage = "http://github.com/gemspec"
  
  s.files = Dir.glob("ext/**/*.{c,rb,h}") +
            Dir.glob("lib/**/*.rb")
  
  s.extensions << "ext/libadyencc/extconf.rb"
  
  s.add_development_dependency "rake-compiler"
end
