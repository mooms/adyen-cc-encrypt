# Adyen Creditcard Encryption

This library can be used for encrypting creditcard details, similar to the way Adyen does in its iOS library. This library can be used in Ruby, which allows for nice integration tests for instance. 
