#include "ruby.h"
#include "encw.h"

static VALUE do_encrypt(VALUE self, VALUE text, VALUE modulus, VALUE exponent);

void Init_libadyencc()
{
  VALUE mAdyen = rb_define_module("Adyen");
  VALUE mCreditcard = rb_define_module_under(mAdyen, "Creditcard");
  rb_define_singleton_method(mCreditcard, "adyen_encrypt", do_encrypt, 3);
}

static VALUE do_encrypt(VALUE self, VALUE text, VALUE exponent, VALUE modulus)
{
  char * p_text;
  char * p_modulus;
  char * p_exponent;
  long res;
  char * outputbuf;
  VALUE result;

  p_text = RSTRING_PTR(text);
  p_modulus = RSTRING_PTR(modulus);
  p_exponent = RSTRING_PTR(exponent);

  outputbuf = NULL;

  res = adyen_encrypt(p_text, p_modulus, p_exponent, &outputbuf);

  if (res != 0) {
    free(outputbuf);

    rb_raise(rb_eException, "Encrypt exception: code %li, err %s", res, getErrorString());
  } else {
    result = rb_str_new2(outputbuf);
    free(outputbuf);
  }

  return result;
}
