#include <stdio.h>
#include <string.h>

const char * getErrorString();
char * getResult();

long adyen_encrypt(const char * plaintext, const char * modulus, const char *exponent, char ** outputbuf);

