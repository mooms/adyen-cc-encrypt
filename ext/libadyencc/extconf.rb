require 'mkmf'

LIBDIR      = RbConfig::CONFIG['libdir']
INCLUDEDIR  = RbConfig::CONFIG['includedir']

HEADER_DIRS = [
  # First search /opt/local for macports
  '/opt/local/include',

  # Then search /usr/local for people that installed from source
  '/usr/local/include',

  # Check the ruby install locations
  INCLUDEDIR,

  # Finally fall back to /usr
  '/usr/include',
]

LIB_DIRS = [
  # First search /opt/local for macports
  '/opt/local/lib',

  # Then search /usr/local for people that installed from source
  '/usr/local/lib',

  # Check the ruby install locations
  LIBDIR,

  # Finally fall back to /usr
  '/usr/lib',
]

# Give it a name
extension_name = 'libadyencc/libadyencc'

# The destination
dir_config("crypto", HEADER_DIRS, LIB_DIRS)
dir_config("ssl", HEADER_DIRS, LIB_DIRS)
dir_config("m", HEADER_DIRS, LIB_DIRS)

$libs += " -lssl -lcrypto -lm"

#$objs = %w{encw} 

unless find_header('openssl/evp.h')
  abort "openssl evp.h is missing.  please check openssl lib"
end

unless find_header('openssl/conf.h')
  abort "openssl conf.h is missing.  please check openssl lib"
end

unless find_header('openssl/err.h')
  abort "openssl err.h is missing.  please check openssl lib"
end

unless find_header('openssl/rsa.h')
  abort "openssl rsa.h is missing.  please check openssl lib"
end

unless find_header('openssl/bio.h')
  abort "openssl bio.h is missing.  please check openssl lib"
end

unless find_header('openssl/sha.h')
  abort "openssl sha.h is missing.  please check openssl lib"
end

unless find_header('openssl/hmac.h')
  abort "openssl hmac.h is missing.  please check openssl lib"
end

unless find_header('openssl/buffer.h')
  abort "openssl buffer.h is missing.  please check openssl lib"
end

unless find_header('openssl/rand.h')
  abort "openssl rand.h is missing.  please check openssl lib"
end

# Do the work
create_makefile(extension_name)
