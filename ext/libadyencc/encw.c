#include "encw.h"

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/buffer.h>
#include <openssl/rand.h>
#include <setjmp.h>
#include <stdlib.h>

#define CUQP const unsigned char * 

int printIntermediateResults(CUQP key, CUQP nonce, CUQP ciphertext, CUQP tag, CUQP encryptedkey, CUQP payload, size_t ciphertext_len, size_t encryptedkey_len) ;

void handleErrors(void);
size_t encryptRsa(const char *exponent, const char *modulus, CUQP plaintext, size_t plaintext_len, unsigned char *out);
size_t base64enc(CUQP input, size_t length, unsigned char *ciphertext);
size_t base64dec(CUQP input, size_t length, unsigned char *out);
int encrypt_payload(CUQP ciphertext, size_t ciphertext_len, CUQP tag, CUQP nonce, CUQP encryptedkey, size_t encryptedkey_len, CUQP key, char ** outputbuf);
int encryptccm(const char *plaintext, size_t plaintext_len, CUQP key, CUQP iv, unsigned char *ciphertext, unsigned char *tag);
void outbuf_append(CUQP data, size_t len, char * outputbuf);

#define VERBOSE 0
#define NONCE_LEN 12 
#define TAG_LEN 8
#define KEY_LEN 32
#define AES_BLOCK_SIZE 256


static jmp_buf buf;
static size_t pos;

long adyen_encrypt(const char * plaintext, const char * modulus, const char *exponent, char ** outputbuf)
{

  /* A 256 bit key */
  unsigned char key[32];

    /* A 96 bit IV/nonce */
  unsigned char nonce[12];

  /* Buffer for tag */
  unsigned char tag[8];

  /* Buffer for ciphertext. Ensure the buffer is long enough for the
   * ciphertext which may be longer than the plaintext, dependant on the
   * algorithm and mode
   */
  unsigned char ciphertext[strlen(plaintext) + AES_BLOCK_SIZE];
  size_t ciphertext_len;

  /* Buffer for encrypted key */
  unsigned char encryptedkey[AES_BLOCK_SIZE];
  size_t encryptedkey_len;

  long error;

  /* Initialise the library */
  ERR_load_crypto_strings();
  
  OpenSSL_add_all_algorithms();
  OPENSSL_config(NULL);

  if (RAND_bytes(key, KEY_LEN)<=0)
    handleErrors();

  if (RAND_bytes(nonce, NONCE_LEN)<=0) 
    handleErrors();

  pos = 0;

  if (!(error = setjmp(buf))) {
    /* Encrypt the plaintext */
    ciphertext_len = encryptccm(plaintext, strlen(plaintext), key, nonce,
      ciphertext, tag);

    encryptedkey_len = encryptRsa(exponent, modulus, key, KEY_LEN, encryptedkey);

    error = encrypt_payload(ciphertext, ciphertext_len, tag, nonce, encryptedkey, encryptedkey_len, key, outputbuf);
  }

  return error;
}

void outbuf_append(CUQP data, size_t len, char * outputbuf) {
  memcpy(outputbuf + pos, data, len);
  pos += len;
}

unsigned int b64size(unsigned int input_size) 
{
  unsigned int adjustment = ( (input_size % 3) ? (3 - (input_size % 3)) : 0);
  unsigned int code_padded_size = ( (input_size + adjustment) / 3) * 4;
  unsigned int newline_size = ((code_padded_size) / 64) * 1;

  return code_padded_size + newline_size;
}

int encrypt_payload(CUQP ciphertext, size_t ciphertext_len, CUQP tag, CUQP nonce, CUQP encryptedkey, size_t encryptedkey_len, CUQP key, char ** outputbuf)
{

  /* Base64 encrypted payload */
  size_t base64payload_len;

  /* Base64 encrypted key */
  unsigned char base64encryptedkey[349];
  size_t base64encryptedkey_len;

  unsigned char payload[NONCE_LEN + ciphertext_len + TAG_LEN];

  base64payload_len = b64size((unsigned int)(NONCE_LEN + ciphertext_len + TAG_LEN));

  *outputbuf = (char *)malloc((base64payload_len + 13 + 1 + 1 + 349) * sizeof(char));

  memcpy(payload, nonce, NONCE_LEN);
  memcpy(payload + NONCE_LEN, ciphertext, ciphertext_len);
  memcpy(payload + NONCE_LEN + ciphertext_len, tag, TAG_LEN);

  base64encryptedkey_len = base64enc(encryptedkey, encryptedkey_len, base64encryptedkey);

  printIntermediateResults(key, nonce, ciphertext, tag, encryptedkey, payload, ciphertext_len, encryptedkey_len);

  outbuf_append((unsigned char *)"adyenio0_1_1$", strlen("adyenio0_1_1$"), *outputbuf);

  outbuf_append(base64encryptedkey, base64encryptedkey_len, *outputbuf);

  outbuf_append((unsigned char *)"$", 1, *outputbuf);

  pos += base64enc(payload, NONCE_LEN + ciphertext_len + TAG_LEN, (unsigned char *)(*outputbuf + pos));

  outbuf_append((unsigned char *)"\0", 1, *outputbuf);

  return 0;
}

int printIntermediateResults(CUQP key, CUQP nonce, CUQP ciphertext, CUQP tag, CUQP encryptedkey, CUQP payload, size_t ciphertext_len, size_t encryptedkey_len) 
{
  if (VERBOSE) {
    printf("Key is:\n");
    BIO_dump_fp(stdout, (char *)key, KEY_LEN);
    
    printf("Nonce is:\n");
    BIO_dump_fp(stdout, (char *)nonce, NONCE_LEN);
    
    printf("Ciphertext is:\n");
    BIO_dump_fp(stdout, (char *)ciphertext, (int)ciphertext_len);

    printf("Tag is:\n");
    BIO_dump_fp(stdout, (char *)tag, TAG_LEN);

    printf("EncryptedKey is:\n");
    BIO_dump_fp(stdout, (char *)encryptedkey, (int)encryptedkey_len);

    printf("Payload is:\n");
    BIO_dump_fp(stdout, (char *)payload, (int)( NONCE_LEN + ciphertext_len + TAG_LEN ) )  ;
  }
  return 0;
}

const char * getErrorString()
{
  const char * res;
  res = ERR_error_string(ERR_get_error(), 0);
  ERR_free_strings();
  return res;
}

void handleErrors(void)
{
  longjmp(buf, 1);
}

size_t encryptRsa(const char *exponent, const char *modulus, CUQP plaintext, size_t plaintext_len, unsigned char *out)
{
  EVP_PKEY_CTX *ctx;
  size_t outlen;

  EVP_PKEY *key;
  RSA* rsa;
  rsa = RSA_new();

  if(!rsa) 
    handleErrors();

  BN_hex2bn(&rsa->n, modulus);
  BN_hex2bn(&rsa->e, exponent);

  key = EVP_PKEY_new();
  if (!key)     
    handleErrors();


  if(EVP_PKEY_set1_RSA(key, rsa) <= 0)
    handleErrors();
    
  ctx = EVP_PKEY_CTX_new(key, NULL);
  if (!ctx)
      handleErrors();

  if (EVP_PKEY_encrypt_init(ctx) <= 0)
      handleErrors();

  if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING) <= 0)
      handleErrors();

  outlen = AES_BLOCK_SIZE;

  if (EVP_PKEY_encrypt(ctx, out, &outlen, plaintext, plaintext_len) <=0)
      handleErrors();

  if (outlen != AES_BLOCK_SIZE)
      handleErrors();

  if (ctx)
    EVP_PKEY_CTX_free(ctx);

  if (key)
    EVP_PKEY_free(key);

  if (rsa)
    RSA_free(rsa);

  return outlen;
}

size_t base64enc(CUQP input, size_t length, unsigned char *ciphertext)
{
  BIO *bmem, *b64;
  BUF_MEM *bptr;
  size_t len;

  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, (int)length);
  if (!BIO_flush(b64))
    handleErrors();

  BIO_get_mem_ptr(b64, &bptr);

  memcpy((void*)ciphertext, bptr->data, bptr->length);

  len = bptr->length;

  ciphertext[len - 1] = 0;

  BIO_free_all(b64);

  return len -1;
}

size_t base64dec(CUQP input, size_t length, unsigned char *out)
{
  BIO *b64, *bmem;
  int size;

  b64 = BIO_new(BIO_f_base64());
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

  bmem = BIO_new_mem_buf((void *)input, (int)length);
  bmem = BIO_push(b64, bmem);

  size = BIO_read(bmem, out, (int)length);

  out[size] = 0;

  BIO_free_all(bmem);
  return size;
}

int encryptccm(const char *plaintext, size_t plaintext_len, CUQP key, CUQP iv,
	unsigned char * ciphertext, unsigned char * tag)
{
	EVP_CIPHER_CTX *ctx;

	int len;

	int ciphertext_len;

	/* Create and initialise the context */
	if(!(ctx = EVP_CIPHER_CTX_new())) 
    handleErrors();

	/* Initialise the encryption operation. */
	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ccm(), NULL, NULL, NULL))
		handleErrors();

	/* Setting IV len to NONCE_LEN. */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_IVLEN, NONCE_LEN, NULL))
		handleErrors();

	/* Set Tag length */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_TAG, TAG_LEN, NULL);

	/* Initialise key and IV */
	if(1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv)) 
    handleErrors();

	/* Provide the total plaintext length
	 */
	if(1 != EVP_EncryptUpdate(ctx, NULL, &len, NULL, (int)plaintext_len))
		handleErrors();

	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can only be called once for this
	 */
	if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, (unsigned char *)plaintext, (int)plaintext_len))
		handleErrors();

	ciphertext_len = len;

	/* Finalise the encryption. Normally ciphertext bytes may be written at
	 * this stage, but this does not occur in CCM mode
	 */
	if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) 
    handleErrors();
	
  ciphertext_len += len;

	/* Get the tag */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_GET_TAG, TAG_LEN, tag))
		handleErrors();

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}
